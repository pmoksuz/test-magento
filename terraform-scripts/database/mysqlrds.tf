resource "aws_db_subnet_group" "my_vpc" {
  name       = "my_vpc"
  subnet_ids = ["subnet-016d6857355c4e340","subnet-0539ded7b195c76a3"]

  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_db_instance" "default" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "admin"
  password             = "secureadmin"
  parameter_group_name = "default.mysql5.7"
  backup_retention_period = 0
  db_subnet_group_name = "${aws_db_subnet_group.my_vpc.name}"
  skip_final_snapshot = true
}