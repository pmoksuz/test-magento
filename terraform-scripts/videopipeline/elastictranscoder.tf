resource "aws_s3_bucket" "inputs" {
  bucket = var.input_bucket
  acl    = "private"
  region   = var.aws_region
}

resource "aws_s3_bucket" "outputs" {
  bucket = var.output_bucket
  acl    = "private"
  region   = var.aws_region
}


resource "aws_elastictranscoder_pipeline" "bar" {
  input_bucket = var.input_bucket
  name         = "aws_elastictranscoder_pipeline_tf_test_"
  role         = var.transcoder_role

  content_config {
    bucket        = var.output_bucket
    storage_class = "Standard"
  }

  thumbnail_config {
    bucket        = var.output_bucket
    storage_class = "Standard"
  }
}