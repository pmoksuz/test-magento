module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "terra-vpc"
  cidr = "10.34.0.0/16"

  azs             = ["us-west-2a", "us-west-2b", "us-west-2c"]
  private_subnets = ["10.34.1.0/24", "10.34.2.0/24", "10.34.3.0/24"]
  public_subnets  = ["10.34.101.0/24", "10.34.102.0/24", "10.34.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}