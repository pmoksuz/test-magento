
variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "us-west-2"
}
variable "transcoder_role" {
  type = string
  default = "arn:aws:iam::830830610667:role/Elastic_Transcoder_Default_Role"
}

variable "input_bucket" {
  type = string
  default = "terrabucket001"
}

variable "input_bucket_arn" {
  type = string
  default = "arn:aws:s3:::terrabucket001"
}
