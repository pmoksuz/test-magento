
variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "us-west-2"
}

variable "image_id" {
  type = string
  default = "ami-0c65ebf4b44038cc4"
}

variable "sg" {
  type = list(string)
  default = ["sg-06d7c50e3fa7c262a"]
}

variable "elbsg" {
  type = list(string)
  default = ["sg-0f848c43c49e467e3"]
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "vpc_zone" {
  type = list(string)
  default = ["subnet-03bf39f60c6802c9d", "subnet-065a05450c0b1f449"]
}