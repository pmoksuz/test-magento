
######
# ELB
######
module "elb" {
  source = "terraform-aws-modules/elb/aws"

  name = "terra-elb-example"

  subnets         = var.vpc_zone
  security_groups = var.elbsg
  internal        = false

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
    },
  ]
  access_logs = {}
  health_check = {
    target              = "TCP:80"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  tags = {
    Owner       = "user"
    Environment = "dev"
  }
}

#####
# ASG EC2
#####
module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"
  
  name = "test-terra-service"

  # Launch configuration
  lc_name = "test-terra-lc"

  image_id        = var.image_id
  instance_type   = var.instance_type
  security_groups = var.sg
  load_balancers  = [module.elb.this_elb_id]
  key_name = "admin_mg"

  # ebs_block_device = [
  #   {
  #     device_name           = "/dev/xvda"
  #     volume_type           = "gp2"
  #     volume_size           = "50"
  #     delete_on_termination = true
  #   },
  # ]

  root_block_device = [
    {
      volume_size = "50"
      volume_type = "gp2"
    },
  ]

  # Auto scaling group
  asg_name                  = "terra-asg"
  vpc_zone_identifier       = var.vpc_zone
  health_check_type         = "EC2"
  min_size                  = 0
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0

  
  tags = [
    {
      key                 = "Environment"
      value               = "dev"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = "megasecret"
      propagate_at_launch = true
    },
  ]

  tags_as_map = {
    extra_tag1 = "extra_value1"
    extra_tag2 = "extra_value2"
  }
}
